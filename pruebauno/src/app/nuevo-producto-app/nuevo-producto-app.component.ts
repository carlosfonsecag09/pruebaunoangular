import { Component, OnInit, Input } from '@angular/core';
import { PropiedadesProducto } from '../models/propiedades-producto.model';

@Component({
  selector: 'app-nuevo-producto-app',
  templateUrl: './nuevo-producto-app.component.html',
  styleUrls: ['./nuevo-producto-app.component.css']
})
export class NuevoProductoAppComponent implements OnInit {
  productos: PropiedadesProducto[];
  constructor() {
    this.productos = [];
   }

  ngOnInit(): void {
  }

  guardar(nombre: string, descripcion: string, precio: string): boolean
  {
    this.productos.push(new PropiedadesProducto(nombre, descripcion, precio));
    return false;
  }
}
