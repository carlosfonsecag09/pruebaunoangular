import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevoProductoAppComponent } from './nuevo-producto-app.component';

describe('NuevoProductoAppComponent', () => {
  let component: NuevoProductoAppComponent;
  let fixture: ComponentFixture<NuevoProductoAppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NuevoProductoAppComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevoProductoAppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
