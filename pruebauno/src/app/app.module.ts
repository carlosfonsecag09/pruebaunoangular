import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NavbarAppComponent } from './navbar-app/navbar-app.component';
import { NuevoProductoAppComponent } from './nuevo-producto-app/nuevo-producto-app.component';
import { ProductoAppComponent } from './producto-app/producto-app.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarAppComponent,
    NuevoProductoAppComponent,
    ProductoAppComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
