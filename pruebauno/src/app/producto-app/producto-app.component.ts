import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { PropiedadesProducto } from '../models/propiedades-producto.model';

@Component({
  selector: 'app-producto-app',
  templateUrl: './producto-app.component.html',
  styleUrls: ['./producto-app.component.css']
})
export class ProductoAppComponent implements OnInit {
  @Input() producto: PropiedadesProducto;
  @HostBinding('attr.class') cssClass = 'col-md-6 col-xl-6';
  constructor() {

   }

  ngOnInit(): void {
  }

}
