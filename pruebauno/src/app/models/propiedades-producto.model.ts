export class PropiedadesProducto{
    nombre: string;
    descripcion: string;
    precio: string;

    constructor(n: string, d: string, p: string){
        this.nombre = n;
        this.descripcion = d;
        this.precio = p;
    }
}
